# Use Neovim as Godot's External Editor 

- Prerequisite: 
    - neovim-remote (install with pip)
    - A plugin manager for neovim (lazy)
    - LSP plugins for neovim (cmp, lspconfig, mason, etc)


## Configure Neovim

1. Install "habamax/vim-godot"
2. Setup LSP:
    ```lua
    local capabilities = vim.lsp.protocol.make_client_capabilities()
    capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)
    
    require'lspconfig'.gdscript.setup{capabilities}
    ```


## Configure Godot

- Editor > Editor Settings > External:

    - Use External Editor: on

    - Exec Path: nvr

    - Exec Flags: --servername /tmp/godot --remote-send "<C-\><C-N>:n {file}<CR>:call cursor({line},{col})<CR>"


## How to Use

- Go to the root of your Godot project (where `project.godot` is)
- Run `nvim --listen /tmp/godot .`

- Double click on a gdscript to open it in neovim

- You don't have to use `/tmp/godot`, but make sure you use the path to that socket in both `nvim --listen` and Exec Path. The vim-godot's guide uses `godothost`, it didn't work for me because nvim will generate a socket in `/run/user/1000/` with `godothost` as a part of the socket's name. And `godothost` in Exec Path will not find a match, therefore will not connect to the nvim process.
