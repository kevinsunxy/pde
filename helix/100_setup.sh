#!/bin/bash

dir=$HOME"/.config/helix"

if [ ! -d "$dir" ]; then
    mkdir -p "$dir"
fi
ln -s ~/pde/helix/config.toml $dir
ln -s ~/pde/helix/languages.toml $dir
