curl -sSL https://install.python-poetry.org | python3 -

# Enable bash tab completion
poetry completions bash >> ~/.bash_completion
