pkg install python-cryptography
pip install poetry

# Enable bash tab completion
poetry completions bash >>~/.bash_completion
