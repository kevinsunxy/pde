#!/bin/bash

installer=dnf

grep ubuntu /etc/os-release > /dev/null


if [ $? -eq 0 ]; then
    installer=apt
fi


sudo $installer install -y kitty tmux htop ripgrep fd-find luarocks

bash ~/pde/bashrc/100_setup.sh
bash ~/pde/kitty/100_setup.sh
bash ~/pde/tmux/100_setup.sh

