-- Kevin Sun
--[[
------------------------------------------------------------------------------
     
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣆⣾⣿⠇⣠⣾⣷⢂⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⢞⡤⣠⣦⣾⣿⣾⣿⣷⣾⣿⣿⣿⣿⠇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⣴⠖⠒⠋⠉⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣭⢶⣶⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⠀⣀⠀⠀⠀⢀⣀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⣈⣭⣿⣤⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⡟⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣶⣠⣾⣿⣶⣿⣿⣷⣶⣴⣿⣿⣿⣿⣖⡀
      ⠀⠀⠀⢠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣌⣿⣿⣿⣿⣿⡿⠿⢻⣿⣿⣿⣿⣿⣟⠛⠀
      ⠀⠀⠀⠈⠹⣛⣻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡛⠇⠀⠀⠀⠀⠀⠀⣀⠀⢀⣤⣤⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⢿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠙⠉⠛⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⢻⣿⣿⣿⡛⠿⠿⠄⣸⣿⣿⣿⣿⣿⣿⣿⣿⣷⠀⠀⠀⣀⡄⢠⣿⣿⣴⣿⣿⣷⣾⣿⡿⠆⠀⠀⠀⠀⠀⠀⠰⣾⣿⣿⣿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠘⢿⣿⣿⣭⢛⣴⣾⣿⣿⣿⣿⣿⣿⣿⣿⡉⠃⠀⠀⢰⣿⣷⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣦⣄⡀⠀⠀⠀⠀⠙⠿⣿⣿⣿⣿⣦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠁⠀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣯⠉⠟⠁⠀⣴⣧⣾⣿ init.lua ⣿⣿⣿⣏⠃⠀⠀⠀⠀⠀⣦⣿⣿⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⣾⣿⣿⣿⣿⣿⣿⣿⣿⠋⠿⠀⠀⣠⣠⣿⣿ Year of Dragon ⣿⣶⡄⠀⠀⠀⠘⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⢠⣾⣶⣤⡀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⡟⠇⢀⡀⢸⣿⣿⣿⣿⣿⣿⣿ Edition ⣿⣿⣿⣿⣿⣧⣤⣤⡄⣰⣧⣼⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠙⣿⣿⣇⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣴⣿⣷⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠛⠋⠉⠙⠻⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⣿⣿⣿⣿⣿⣿⣿⣿⠇⠀⠀⠀⠀⠀⠀⠀⠀
      ⠐⠞⠛⠛⠻⢷⣦⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⠀⢀⣤⡄⠹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠙⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⠀⠀⠀⠚⣿⣿⣤⣈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠙⠺⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⢨⣿⠏⠙⠛⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠉⢻⣷⣶⣤⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⠻⠿⣿⣿⣿⣿⣿⣿⠿⠛⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠚⠁⠀⠀⠀⠀⠀⠈⠙⠛⠛⠛⠛⠋⠉⠀⠀⠀⢸⣿⡏⠙⠇⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣿⠟⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⠛⠃⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠁⠀⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
-------------------------------------------------------------------------------
Art by https://emojicombos.com/dragon-ascii-art

References: 
  - kickstart.nvim
  - lazy.nvim
  - LazyVim
  - https://github.com/ThePrimeagen/neovimrc/tree/master
  - https://github.com/dam9000/kickstart-modular.nvim/tree/master
--]]

-- TODO: testing

require("ks")

--[[
Note to future self:

  - Loading mason on the VeryLazy event now causes two problems:
      1. mason-tools-installer won't auto install
      2. Not triggering the LspAttach event, thus not setting lsp keymaps
      - So mason is loaded on start up now.

  - Lsp keymaps are set using autocommand instead of using on_attach 
    (thx primeagen)

  - The files (except init.lua) in keymaps should return a keymap_groups table, 
    and you later use keymaps.keymapper.set_keys to explicitly set keys. 
    This is a design choice to keep apis uniform (set imperatively and on event

  - Some keymaps are set in plugin configs due to api decisions of the plugin 
    authors (cmp, treesitter, etc)

  - Did my current best to abstract the lsp/formatter/linter of languages to one
    single location (ks.language_toolings) and use adapter functions to comply 
    with different plugin apis. Try to use that instead whenever you can.

  - If you run into some tricky situations with lsp/linter etc, check these:

    - https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
    - Find the option you want on language server official doc.
      Then translate it into nvim lspconfig compatible format.

    - Read conform builtin vimdoc
    - Check the doc of the formatter for args, and override defaults of conform

    - Check out nvim-lint github page for doc
    - Read official docs of the linter for args

    - Sometimes, a tool just can't do what you want. 
      In that case, try something else.
      For example, pyright's linting diagnostics cannot be disabled. 
      So in order to use ruff for linting, I had to switch to jedi.
      (Tried others, jedi's the best at the moment.)

  - This config was split to mutiple files entirely using helix! :)
    Configuring helix made me understand lsp and other tools a lot better.
    Without helix, this rewrite wouldn't be possible. Really awesome editor.

  - DAP is such a pain in the A to set up. I will spare you from the rant. 
    1. Don't use mason to install DAPs, install them per project.
    2. Use nvim-dap with nvim installed inside a container is not possible.
    3. DO NOT use any language specific plugin for nvim-dap. 

    - The points 1 and 2 are all related to python's virtual env and how difficult it is to make nvim-dap find the correct python and debugpy.

    - When you need to add a new DAP, read the nvim-dap's wiki, grab the code, and put it in a table similar to how I did for python and go now. It is way easier than using plugins that have different apis.
--]]
