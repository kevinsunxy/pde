
local custom_commands = {
	reload_config = {
		name = "ReloadConfig",
		command = "source $MYVIMRC",
	},
	edit_config = {
		name = "EditConfig",
		command = "edit $MYVIMRC",
	},
}

local function add_custom_commands(custom_commands)
	for _, cmd in pairs(custom_commands) do
		local opts = cmd.opts and cmd.opts or {}
		vim.api.nvim_create_user_command(cmd.name, cmd.command, opts)
	end
end

add_custom_commands(custom_commands)
