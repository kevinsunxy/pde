local ensure_installed = {
	"vim",
	"vimdoc",

	"lua",
	"python",
	"htmldjango",
	"go",
	"gdscript",
	"godot_resource",
	"javascript",
	"typescript",
	"html",
	"css",
	"sql",

	"yaml",
	"toml",
	"markdown",
	"markdown_inline",
	"bash",
	"json",
	"jq",
	"csv",
	"dockerfile",
	"nix",
	"git_config",
	"gitignore",
	"regex",
}

local textobjects = {
	select = {
		enable = true,
		lookahead = true,
		keymaps = {
			["aa"] = "@parameter.outer",
			["ia"] = "@parameter.inner",
			["af"] = "@function.outer",
			["if"] = "@function.inner",
			["ac"] = "@class.outer",
			["ic"] = "@class.inner",
		},
	},
	move = {
		enable = true,
		set_jumps = true,
		goto_next_start = {
			["<leader>nm"] = "@function.outer",
			["<leader>nc"] = "@class.outer",
		},
		goto_next_end = {
			["<leader>nM"] = "@function.outer",
			["<leader>nC"] = "@class.outer",
		},
		goto_previous_start = {
			["<leader>pm"] = "@function.outer",
			["<leader>pc"] = "@class.outer",
		},
		goto_previous_end = {
			["<leader>pM"] = "@function.outer",
			["<leader>pC"] = "@class.outer",
		},
	},
	swap = {
		enable = true,
		swap_next = {
			["<S-l>"] = "@parameter.inner",
		},
		swap_previous = {
			["<S-h>"] = "@parameter.inner",
		},
	},
}

local treesitter = {
	"nvim-treesitter/nvim-treesitter",
	dependencies = {
		"nvim-treesitter/nvim-treesitter-textobjects",
	},
	build = ":TSUpdate",

	event = { "BufReadPost", "BufNewFile" },

	config = function()
		local configs = require("nvim-treesitter.configs")
		configs.setup({
			ensure_installed = ensure_installed,
			highlight = { enable = true },
			indent = { enable = true },
			incremental_selection = {
				enable = true,
				keymaps = {
					init_selection = "<Enter>",
					node_incremental = "<Enter>",
					scope_incremental = "<C-Enter>",
					node_decremental = "<BS>",
				},
			},
			textobjects = textobjects,
		})
	end,
}

return treesitter
