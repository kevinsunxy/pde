local plugins = {

	colorscheme = {
		"rose-pine/neovim",
		name = "rose-pine",
		lazy = false,
		priority = 1000,
		config = function()
			vim.cmd("colorscheme rose-pine-moon")
		end,
	},

	git_integration = {
		"tpope/vim-fugitive",
		cmd = { "G", "Git" },
	},

	git_signs = {
		"lewis6991/gitsigns.nvim",
		event = "BufReadPre",
		opts = {
			signs = {
				add = { text = "+" },
				change = { text = "~" },
				delete = { text = "_" },
				topdelete = { text = "‾" },
				changedelete = { text = "~" },
			},
		},
	},

	auto_set_correct_indentation = {
		"tpope/vim-sleuth",
		event = "VeryLazy",
	},

	comment = {
		"numToStr/Comment.nvim",
		event = "VeryLazy",
		opts = {},
	},

	indentation_guide_line = {
		"lukas-reineke/indent-blankline.nvim",
		event = "BufReadPost",
		main = "ibl",
		opts = {},
	},

	status_line = {
		"nvim-lualine/lualine.nvim",
		event = "VeryLazy",
		opts = {
			options = {
				icons_enabled = false,
			},
		},
	},

	keymap_cheatsheet = {
		"folke/which-key.nvim",
		event = "VeryLazy",
		config = function()
			require("which-key").register({
				["<leader>a"] = { name = "[A]ction", _ = "which_key_ignore" },
				["<leader>d"] = { name = "[D]ocument", _ = "which_key_ignore" },
				["<leader>D"] = { name = "[D]ebug", _ = "which_key_ignore" },
				["<leader>r"] = { name = "[R]ename", _ = "which_key_ignore" },
				["<leader>g"] = { name = "[G]rep", _ = "which_key_ignore" },
				["<leader>s"] = { name = "[S]earch", _ = "which_key_ignore" },
				["<leader>t"] = { name = "[T]oggle", _ = "which_key_ignore" },
				["<leader>w"] = { name = "[W]orkspace", _ = "which_key_ignore" },
				["<leader>n"] = { name = "[N]ext", _ = "which_key_ignore" },
				["<leader>p"] = { name = "[P]rev", _ = "which_key_ignore" },
			})
		end,
	},

	fuzzy_finder = {
		"nvim-telescope/telescope.nvim",
		branch = "0.1.x",
		dependencies = {
			"nvim-lua/plenary.nvim",
			{
				"nvim-telescope/telescope-fzf-native.nvim",
				build = "make",
				cond = function()
					local has_make_installed = vim.fn.executable("make") == 1
					return has_make_installed
				end,
			},
		},

		event = "VeryLazy",

		config = function()
			require("telescope").setup({
				defaults = {

					mappings = {
						i = {
							["<C-u>"] = false,
							["<C-d>"] = false,
						},
					},
				},
			})

			pcall(require("telescope").load_extension, "fzf")

			local keymapper = require("ks.keymaps.keymapper")
			local keys = require("ks.keymaps.telescope")
			keymapper.set_keymaps(keys)
		end,
	},
}

return vim.tbl_values(plugins)
