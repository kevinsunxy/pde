return {
	"mfussenegger/nvim-lint",
	event = {
		"BufReadPre",
		"BufNewFile",
	},
	config = function()
    local toolings = require("ks.language_toolings")
		require("lint").linters_by_ft = toolings:linters()

		local lint = function()
			require("lint").try_lint()
		end
		vim.api.nvim_create_autocmd({ "BufWritePost", "InsertLeave" }, {
			callback = lint,
		})
	end,
}
