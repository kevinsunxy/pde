return {
		"stevearc/conform.nvim",
		opts = {},
		event = {
			"BufReadPre",
			"BufNewFile",
		},
		config = function()
      local toolings = require("ks.language_toolings")
			require("conform").setup({
				formatters_by_ft = toolings:formatters(),
				format_on_save = {
					timeout_ms = 500,
					lsp_fallback = true,
				},
			})
		end,
	}
