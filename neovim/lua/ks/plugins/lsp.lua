local function mason_config()
	local toolings = require("ks.language_toolings")

	require("mason").setup()

	local mason_lspconfig = require("mason-lspconfig")
	require("neodev").setup()

	local capabilities = vim.lsp.protocol.make_client_capabilities()
	capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

	mason_lspconfig.setup_handlers({
		function(server_name)
			local settings = toolings:language_server_settings()[server_name]

			local window_opts = { border = "double", max_width = 100 }

			local handlers = {
				["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, window_opts),
				["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, window_opts),
			}

			local opts = {
				capabilities = capabilities,
				settings = settings,
				filetypes = (toolings:filetypes()[server_name] or {}).filetypes,
				handlers = handlers,
			}

			-- special treatment for c_sharp, it requires setting cmd manually in config
			if
				toolings:language_server_settings()[server_name]
				and toolings:language_server_settings()[server_name].cmd
			then
				opts.cmd = toolings:language_server_settings()[server_name].cmd
			end

			require("lspconfig")[server_name].setup(opts)
		end,
	})

	vim.diagnostic.config({
		virtual_text = false,
		float = {
			focusable = false,
			style = "minimal",
			border = "double",
			source = true,
		},
	})
end

local function cmp_config()
	local cmp = require("cmp")
	local luasnip = require("luasnip")
	require("luasnip.loaders.from_vscode").lazy_load()
	luasnip.config.setup({})

	cmp.setup({
		window = {
			completion = cmp.config.window.bordered({ border = "double" }),
			documentation = cmp.config.window.bordered({ border = "double" }),
		},
		snippet = {
			expand = function(args)
				luasnip.lsp_expand(args.body)
			end,
		},
		completion = {
			completeopt = "menu,menuone,noinsert",
		},
		mapping = cmp.mapping.preset.insert({
			["<C-n>"] = cmp.mapping.select_next_item(),
			["<C-p>"] = cmp.mapping.select_prev_item(),
			["<C-b>"] = cmp.mapping.scroll_docs(-4),
			["<C-f>"] = cmp.mapping.scroll_docs(4),
			["<C-Space>"] = cmp.mapping.complete({}),
			["<CR>"] = cmp.mapping.confirm({
				behavior = cmp.ConfirmBehavior.Replace,
				select = true,
			}),
			["<Tab>"] = cmp.mapping(function(fallback)
				if cmp.visible() then
					cmp.select_next_item()
				elseif luasnip.expand_or_locally_jumpable() then
					luasnip.expand_or_jump()
				else
					fallback()
				end
			end, { "i", "s" }),
			["<S-Tab>"] = cmp.mapping(function(fallback)
				if cmp.visible() then
					cmp.select_prev_item()
				elseif luasnip.locally_jumpable(-1) then
					luasnip.jump(-1)
				else
					fallback()
				end
			end, { "i", "s" }),
		}),
		sources = {
			{ name = "nvim_lsp" },
			{ name = "luasnip" },
			{ name = "path" },
		},
	})
end

return {
	{
		"hrsh7th/nvim-cmp",
		event = "InsertEnter",
		dependencies = {
			{
				"L3MON4D3/LuaSnip",
				build = (function()
					local is_platform_not_supported = vim.fn.has("win32") == 1
					if is_platform_not_supported then
						return
					end
					return "make install_jsregexp"
				end)(),
			},
			"saadparwaiz1/cmp_luasnip",
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-path",
			"rafamadriz/friendly-snippets",
		},
		config = cmp_config,
	},

	{
		"neovim/nvim-lspconfig",
		dependencies = {
			{ "williamboman/mason.nvim", config = true },
			"williamboman/mason-lspconfig.nvim",
			{
				"WhoIsSethDaniel/mason-tool-installer.nvim",
				config = function()
					local toolings = require("ks.language_toolings")
					require("mason-tool-installer").setup({
						ensure_installed = toolings:ensure_installed(),
					})
				end,
			},
			{ "j-hui/fidget.nvim", opts = {} },
			"folke/neodev.nvim",
			"hrsh7th/cmp-nvim-lsp",
		},
		config = mason_config,
	},
}
