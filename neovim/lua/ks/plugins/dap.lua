
return {
  "mfussenegger/nvim-dap",
  
  event = "VeryLazy",
    
  dependencies = {
    "rcarriga/nvim-dap-ui",    
  },
  config = function()
    require("dapui").setup()

    local keymapper =  require("ks.keymaps.keymapper")
    keymapper.set_keymaps(require("ks.keymaps.dap"))

    local daps = require("ks.language_toolings"):daps()

    for lang, dap in pairs(daps) do
      require("dap").adapters[lang] = dap.adapter
      require("dap").configurations[lang] = vim.tbl_values(dap.configs)
    end

  end
  
}
