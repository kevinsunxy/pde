local M = {}

M.languages = {
	c_sharp = {
		ensure_installed = { "omnisharp" },

		filetypes = { "cs" },

		language_servers = {

			-- special treatment for c_sharp, it requires setting cmd manually in config
			omnisharp = {
				cmd = {
					vim.fn.trim(vim.fn.system("which dotnet")), -- Finds dotnet dynamically
					vim.fn.stdpath("data") .. "/mason/packages/omnisharp/libexec/OmniSharp.dll",
				},
				settings = {
					enable_roslyn_analyzers = true,
					organize_imports_on_format = true,
					-- No `cmd` here!
				},
			},
		},
	},

	python = {
		ensure_installed = { "jedi-language-server", "ruff" },

		filetypes = { "python" },

		language_servers = {
			jedi_language_server = {},
		},

		linters = { "ruff" },

		formatters = { "ruff_format", "ruff_fix" },

		dap = {
			adapter = function(callback, config)
				if config.request == "attach" then
					local port = (config.connect or config).port
					local host = (config.connect or config).host or "127.0.0.1"
					callback({
						type = "server",
						port = assert(port, "`connect.port` is required for a python `attach` configuration"),
						host = host,
						options = {
							source_filetype = "python",
						},
					})
				else
					callback({
						type = "executable",
						command = os.getenv("VIRTUAL_ENV") .. "/bin/python",
						args = { "-m", "debugpy.adapter" },
						options = {
							source_filetype = "python",
						},
					})
				end
			end,

			configs = {
				debug_file = {
					type = "python",
					request = "launch",
					name = "Debug file",
					program = "${file}",
				},
			},
		},
	},

	go = {
		ensure_installed = { "gopls", "golangci-lint" },
		language_servers = {
			gopls = {},
		},

		linters = { "golangcilint" },

		formatters = { "gofmt", "goimports" },

		dap = {
			adapter = {
				type = "server",
				port = "${port}",
				executable = {
					command = "dlv",
					args = { "dap", "-l", "127.0.0.1:${port}" },
				},
			},

			configs = {

				debug_file = {
					type = "go",
					name = "Debug",
					request = "launch",
					program = "${file}",
				},
				debug_test = {
					type = "go",
					name = "Debug test",
					request = "launch",
					mode = "test",
					program = "${file}",
				},
				debug_test_go_mod = {
					type = "go",
					name = "Debug test (go.mod)",
					request = "launch",
					mode = "test",
					program = "./${relativeFileDirname}",
				},
			},
		},
	},

	lua = {

		ensure_installed = { "luacheck", "lua_ls", "stylua" },

		language_servers = {
			lua_ls = {
				Lua = {
					-- use luacheck instead
					diagnostics = {
						enable = false,
					},
				},
			},
		},

		linters = { "luacheck" },

		formatters = { "stylua" },
	},
}

function M.ensure_installed(self)
	local ret = {}
	for _, lang in pairs(self.languages) do
		for _, install in pairs(lang.ensure_installed) do
			table.insert(ret, install)
		end
	end
	return ret
end

function M.language_server_settings(self)
	local ret = {}
	for _, lang in pairs(self.languages) do
		ret = vim.tbl_deep_extend("force", ret, lang.language_servers)
	end

	return ret
end

function M._get_options(self, key)
	local ret = {}
	for k, v in pairs(self.languages) do
		ret[k] = v[key]
	end

	return ret
end

function M.filetypes(self)
	return M:_get_options("filetypes")
end
function M.linters(self)
	return M:_get_options("linters")
end

function M.formatters(self)
	return M:_get_options("formatters")
end

function M.daps(self)
	return M:_get_options("dap")
end

return M
