local M = {}

function M.set_leader_key(leader)
	vim.g.mapleader = leader
	vim.g.maplocalleader = leader
	vim.keymap.set({ "n", "v" }, leader, "<Nop>", { silent = true })
end

function M.set_keymaps(keymap_groups, bufnr)
	for _, groups in pairs(keymap_groups) do
		for _, keymap in pairs(groups) do
			local opts = { desc = keymap.desc }

			if bufnr ~= nil then
				opts = vim.tbl_extend("error", opts, { buffer = bufnr })
			end

			vim.keymap.set(keymap.mode, keymap.lhs, keymap.rhs, opts)
		end
	end
end

return M
