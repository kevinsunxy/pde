local dap = require("dap")
local dapui = require("dapui")
local toolings = require("ks.language_toolings")

return {
  start_debugging = {
		file = { 
			mode = { "n" },
			lhs = "<leader>Df",
			rhs = function()

        local lang = toolings.languages[vim.bo.filetype]
        local config = lang.dap.configs.debug_file
        dap.run(config)
        dapui.open()

      end,
			desc = "[F]ile",
    },

		choose = { 
			mode = { "n" },
			lhs = "<leader>Dc",
			rhs = function()
        dap.continue()
        dapui.open()
      end,

			desc = "[C]ontinue/[C]hoose config" 
    },
  },

  stop_debugging = {
    _ = { 
  		mode = { "n" },
  		lhs = "<leader>Ds",
  		rhs = function()
        dap.terminate()
        dapui.close()
      end,
  		desc = "[S]top" 
    },    
  },

  breakpoints = {
    toggle = {
    	mode = { "n" },
  		lhs = "<leader>tb",
  		rhs = dap.toggle_breakpoint,
  		desc = "[B]reakpoint" ,
    },
    set_exception_breakpoints = {
      mode = { "n" },
  		lhs = "<leader>De",
  		rhs = function() dap.set_exception_breakpoints("default") end,
  		desc = "Set [E]xception breakpoints" ,
    },
    clear_breakpoints = {
      mode = { "n" },
  		lhs = "<leader>DB",
  		rhs = dap.clear_breakpoints,
  		desc = "Clear [B]reakpoints" ,
    },
  },

  ui = {
    toggle = {
      mode = { "n" },
  		lhs = "<leader>td",
  		rhs = dapui.toggle,
  		desc = "[D]ebug UI" ,
    }
  }
}
