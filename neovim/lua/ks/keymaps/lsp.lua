local telescope = require("telescope.builtin")

return {
	gotos = {
		definition = {
			mode = { "n" },
			lhs = "gd",
			rhs = telescope.lsp_definitions,
			desc = "[G]oto [D]efinition",
		},
	},
	search = {
		references = {
			mode = { "n" },
			lhs = "<leader>sr",
			rhs = telescope.lsp_references,
			desc = "[R]eferences",
		},
		implementations = {
			mode = { "n" },
			lhs = "<leader>si",
			rhs = telescope.lsp_implementations,
			desc = "[I]plementations",
		},
	},
	list_symbols = {
		document = {
			mode = { "n" },
			lhs = "<leader>ds",
			rhs = telescope.lsp_document_symbols,
			desc = "[S]ymbols",
		},
		workspace = {
			mode = { "n" },
			lhs = "<leader>ws",
			rhs = telescope.lsp_dynamic_workspace_symbols,
			desc = "[S]ymbols",
		},
	},
	list_diagnostics = {
		document = {
			mode = { "n" },
			lhs = "<leader>dd",
			rhs = function()
				telescope.diagnostics({ bufnr = 0 })
			end,
			desc = "[D]iagnostics",
		},

		workspace = {
			mode = { "n" },
			lhs = "<leader>wd",
			rhs = telescope.diagnostics,
			desc = "[D]iagnostics",
		},
	},

	actions = {
		rename_symbol = {
			mode = { "n" },
			lhs = "<leader>rs",
			rhs = vim.lsp.buf.rename,
			desc = "[S]ymbol",
		},
		code_action = {
			mode = { "n" },
			lhs = "<leader>ac",
			rhs = function()
				vim.lsp.buf.code_action({ context = { only = { "quickfix", "refactor", "source" } } })
			end,
			desc = "[C]ode",
		},
	},

	access_docs = {
		hover_doc = {
			mode = { "n" },
			lhs = "<S-k>",
			rhs = vim.lsp.buf.hover,
			desc = "Hover documentation",
		},

		signature_doc = {
			mode = { "n" },
			lhs = "<C-k>",
			rhs = vim.lsp.buf.signature_help,
			desc = "Signature help",
		},
	},

	diagnostics = {
		go_to_next = {
			mode = { "n" },
			lhs = "<leader>nd",
			rhs = vim.diagnostic.goto_next,
			desc = "[D]iagnostic",
		},
		go_to_prev = {
			mode = { "n" },
			lhs = "<leader>pd",
			rhs = vim.diagnostic.goto_prev,
			desc = "[D]iagnostic",
		},
		show_in_list = {
			mode = { "n" },
			lhs = "<leader>q",
			rhs = vim.diagnostic.setloclist,
			desc = "[Q]uickfix Diagnostic",
		},
		show_in_float_windown = {
			mode = { "n" },
			lhs = "<leader>l",
			rhs = vim.diagnostic.open_float,
			desc = "[L]ine Diagnostics",
		},
	},
}
