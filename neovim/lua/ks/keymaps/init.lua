local keymapper = require("ks.keymaps.keymapper")

local leader = " "
keymapper.set_leader_key(leader)

keymapper.set_keymaps(require("ks.keymaps.base"))

