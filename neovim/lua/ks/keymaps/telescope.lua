local telescope = require("telescope.builtin")

local function find_git_root()
	local current_file = vim.api.nvim_buf_get_name(0)
	local current_dir
	local cwd = vim.fn.getcwd()
	if current_file == "" then
		current_dir = cwd
	else
		current_dir = vim.fn.fnamemodify(current_file, ":h")
	end

	local git_root =
		vim.fn.systemlist("git -C " .. vim.fn.escape(current_dir, " ") .. " rev-parse --show-toplevel")[1]
	if vim.v.shell_error ~= 0 then
		print("Not a git repository. Searching on current working directory")
		return cwd
	end
	return git_root
end


return {
	find_file = {
		file_in_cwd = {
			mode = { "n" },
			lhs = "<leader>sf",
			rhs = telescope.find_files,
			desc = "[F]iles",
		},
		most_recently_used = {
			mode = { "n" },
			lhs = "<leader>su",
			rhs = telescope.oldfiles,
			desc = "[U]sed Files",
		},
		git_file = {
			mode = { "n" },
			lhs = "<leader>sg",
			rhs = telescope.git_files,
			desc = "[G]it Files",
		},
	},
	find_buffer = {
		opened_buffer = {
			mode = { "n" },
			lhs = "<leader>sb",
			rhs = telescope.buffers,
			desc = "[B]uffers",
		},
	},
	search_content = {
		current_buffer_fuzzy = {
			mode = { "n" },
			lhs = "<leader>/",
			rhs = telescope.current_buffer_fuzzy_find,
			desc = "[/] Fuzzily Search in Current Buffer",
		},
		live_grep_only_in_opened_files = {
			mode = { "n" },
			lhs = "<leader>gb",
			rhs = function()
				  telescope.live_grep({
					grep_open_files = true,
					prompt_title = "Live Grep (Buffers)",
				})
			end,
			desc = "[B]uffers",
		},
		live_grep_in_cwd = {
			mode = { "n" },
			lhs = "<leader>gd",
			rhs = function()
				  telescope.live_grep({
					prompt_title = "Live Grep (CWD)",
				})
			end,
			desc = "[D]irectory",
		},
		live_grep_on_git_root = {
			mode = { "n" },
			lhs = "<leader>gg",
			rhs = function()
				local git_root = find_git_root()
				if git_root then
					  telescope.live_grep({
						search_dirs = { git_root },
						prompt_title = "Live Grep (Git Root)",
					})
				end
			end,
			desc = "[G]it Root",
		},
		word_under_cursor = {
			mode = { "n" },
			lhs = "<leader>*",
			rhs = telescope.grep_string,
			desc = "Search Word Under Cursor",
		},
		builtin_pickers = {
			mode = { "n" },
			lhs = "<leader>st",
			rhs = telescope.builtin,
			desc = "[T]elescope Pickers",
		},
	},

	misc = {
		help = {
			mode = { "n" },
			lhs = "<leader>sh",
			rhs =  telescope.help_tags,
			desc = "[H]elp",
		},
		keymaps = {
			mode = { "n" },
			lhs = "<leader>sk",
			rhs =  telescope.keymaps,
			desc = "[K]eymaps",
		},
		resume_search = {
			mode = { "n" },
			lhs = "<leader><Space>",
			rhs = telescope.resume,
			desc = "Resume Last Search",
		},
	},
}

