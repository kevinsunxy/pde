return {
	switch_buffers = {
		next_buffer = {
			mode = { "n" },
			lhs = "<Tab>",
			rhs = "<CMD>bn<CR>",
			desc = "Next Buffer",
		},
		prev_buffer = {
			mode = { "n" },
			lhs = "<S-Tab>",
			rhs = "<CMD>bp<CR>",
			desc = "Prev Buffer",
		},
	},
	window_navigation = {
		left = {
			mode = { "n" },
			lhs = "<C-h>",
			rhs = "<C-w>h",
			desc = "To Left Window",
		},
		down = {
			mode = { "n" },
			lhs = "<C-j>",
			rhs = "<C-w>j",
			desc = "To Window Below",
		},
		up = {
			mode = { "n" },
			lhs = "<C-k>",
			rhs = "<C-w>k",
			desc = "To Window Above",
		},
		right = {
			mode = { "n" },
			lhs = "<C-l>",
			rhs = "<C-w>l",
			desc = "To Right Window",
		},
	},
	window_resizing = {
		left = {
			mode = { "n" },
			lhs = "<Left>",
			rhs = "<CMD>vertical resize +2<CR>",
			desc = "Window Resize Left",
		},
		down = {
			mode = { "n" },
			lhs = "<Down>",
			rhs = "<CMD>resize -2<CR>",
			desc = "Window Resize Down",
		},
		up = {
			mode = { "n" },
			lhs = "<Up>",
			rhs = "<CMD>resize +2<CR>",
			desc = "Window Resize Up",
		},
		right = {
			mode = { "n" },
			lhs = "<Right>",
			rhs = "<CMD>vertical resize -2<CR>",
			desc = "Window Resize Right",
		},
	},
	search_highlighting = {
		toggle = {
			mode = { "n" },
			lhs = "<leader>th",
			rhs = "<cmd>set hls!<cr>",
			desc = "Search [H]ighlighting",
		},
	},
	spell_check = {
		toggle = {
			mode = { "n" },
			lhs = "<leader>ts",
			rhs = "<cmd>set spell!<cr>",
			desc = "[S]pell Check",
		},
		next_misspelled_word = {
			mode = { "n" },
			lhs = "<leader>nw",
			rhs = "]s",
			desc = "Misspelled [W]ord",
		},
		prev_misspelled_word = {
			mode = { "n" },
			lhs = "<leader>pw",
			rhs = "[s",
			desc = "Misspelled [W]ord",
		},
	},
	no_EX_mode = {
		{
			mode = { "n" },
			lhs = "gQ",
			rhs = "<Nop>",
			desc = "EX mode? No, thanks",
		},
		{
			mode = { "n" },
			lhs = "q:",
			rhs = "<Nop>",
			desc = "EX mode? No, thanks",
		},
		{
			mode = { "n" },
			lhs = "q/",
			rhs = "<Nop>",
			desc = "EX mode? No, thanks",
		},
		{
			mode = { "n" },
			lhs = "q?",
			rhs = "<Nop>",
			desc = "EX mode? No, thanks",
		},
	},
	bubbling_text = {
		one_line_up = {
			mode = { "v" },
			lhs = "<S-k>",
			rhs = ":m '<-2<CR>gv=gv",
			desc = "Shift Text One Line Up",
		},
		one_line_down = {
			mode = { "v" },
			lhs = "<S-j>",
			rhs = ":m '>+1<CR>gv=gv",
			desc = "Shift Text One Line Down",
		},
	},
}
