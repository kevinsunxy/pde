local option_groups = {
	line_number = {
		number = true,
		relativenumber = true,
	},
	tabs = {
		expandtab = true,
		shiftwidth = 4,
		tabstop = 4,
	},
	code_folding = {
		foldmethod = "expr",
		foldexpr = "nvim_treesitter#foldexpr()",
		foldenable = false,
	},
	split_window_position = {
		splitbelow = true,
		splitright = true,
	},
	supportive_files = {
		swapfile = false,
		undofile = true,
	},
	visual_tweaks = {
		signcolumn = "yes",
		cursorline = true,
		scrolloff = 8,
		termguicolors = true,
		breakindent = true,
	},
	shorter_update_time = {
		updatetime = 250,
		timeoutlen = 300,
	},
	better_completion = {
		completeopt = "menuone,noselect",
	},
	misc = {
		smartindent = true,
		clipboard = "unnamedplus",
		mouse = "a",
	},
}

local function set_options(option_groups)
	for _, groups in pairs(option_groups) do
		for k, v in pairs(groups) do
			vim.opt[k] = v
		end
	end

	vim.opt.iskeyword:append("-") -- treat words like pick-me-up as one word
end


set_options(option_groups)
