local augroup = vim.api.nvim_create_augroup

local KSGroup = augroup("KSGroup", {})

local autocommands = {
	set_lsp_keymaps = {
		events = { "LspAttach" },
		opts = {
			group = KSGroup,
			callback = function(e)
				local keymapper = require("ks.keymaps.keymapper")
				keymapper.set_keymaps(require("ks.keymaps.lsp"), e.buf)
			end,
		},
	},

	line_diagnostics_in_float_win = {
		events = { "CursorHold" },
		opts = {
			group = KSGroup,
			callback = vim.diagnostic.open_float,
		},
	},
}

local function create_autocommands(autocommands)
	local autocmd = vim.api.nvim_create_autocmd
	for _, cmd in pairs(autocommands) do
		autocmd(cmd.events, cmd.opts)
	end
end

create_autocommands(autocommands)
