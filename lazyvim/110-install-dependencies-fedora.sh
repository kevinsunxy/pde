#!/bin/bash
sudo dnf copr enable atim/lazygit -y
sudo dnf install -y ripgrep fd-find lazygit nodejs cargo
pip install pynvim neovim-remote
