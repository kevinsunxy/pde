-- Add/Override Settings
return {
	-- Start
	----------------------------------------------------------------------------------------------------
	-- LSP
	--------------------------------------------------------------------------------
	-- Mason
	------------------------------------------------------------
	{
		"williamboman/mason.nvim",
		opts = {
			-- Language servers should be added with nvim-lspconfig
			-- Linters, formatters go here
			ensure_installed = {
				-- Shell
				"shellcheck",
				"shfmt",

				-- Lua
				"stylua",

				-- Python
				"flake8",
				"black",
				"isort",
				"djlint",

				-- HTML CSS JS
				"prettier",
			},
		},
	},

	-- LSP Config
	------------------------------------------------------------
	{
		"neovim/nvim-lspconfig",
		---@class PluginLspOpts
		opts = {
			---@type lspconfig.options
			-- Language servers go here
			-- They will be automatically installed with mason and loaded with lspconfig
			servers = {
				pyright = {},
				-- Nix
				rnix = {},
			},
		},
	},
	-- Null LS
	------------------------------------------------------------
	{
		"jose-elias-alvarez/null-ls.nvim",
		event = { "BufReadPre", "BufNewFile" },
		dependencies = { "mason.nvim" },
		opts = function()
			local nls = require("null-ls")
			return {
				root_dir = require("null-ls.utils").root_pattern(".null-ls-root", ".neoconf.json", "Makefile", ".git"),
				sources = {
					-- bash
					nls.builtins.diagnostics.shellcheck,
					nls.builtins.formatting.shfmt,

					-- Lua
					nls.builtins.formatting.stylua,

					-- Python
					nls.builtins.diagnostics.flake8,
					nls.builtins.formatting.black,
					nls.builtins.formatting.isort,
					nls.builtins.formatting.djlint,

					-- HTML, CSS, JS
					nls.builtins.formatting.prettier.with({
						disabled_filetypes = { "markdown" },
					}),
				},
			}
		end,
	},
	-- Treesitter
	--------------------------------------------------------------------------------
	{
		"nvim-treesitter/nvim-treesitter",
		opts = {
			ensure_installed = {
				"bash",
				"html",
				"javascript",
				"json",
				"lua",
				"markdown",
				"markdown_inline",
				"python",
				"query",
				"regex",
				"tsx",
				"typescript",
				"vim",
				"yaml",
			},
		},
	},
	----------------------------------------------------------------------------------------------------
	-- End
}
