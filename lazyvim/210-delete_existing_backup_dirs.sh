#!/bin/bash
rm -rf ~/.config/nvim.bak

rm -rf ~/.local/share/nvim.bak
rm -rf ~/.local/state/nvim.bak
rm -rf ~/.cache/nvim.bak
