#!/bin/bash
# Plugins
ln -s "$(pwd)/ks.lua" ~/.config/nvim/lua/plugins/ks.lua

# Keymaps
rm ~/.config/nvim/lua/config/keymaps.lua
ln -s "$(pwd)/keymaps.lua" ~/.config/nvim/lua/config/keymaps.lua
